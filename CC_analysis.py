



from graphframes import *
import sys
from pyspark.sql import SparkSession
from pyspark import SparkContext
from pyspark.sql import SQLContext
#from graphframes.examples import Graphs
import pyspark.sql.functions as func
from pyspark.sql.window import Window
from pyspark.sql import Row
from pyspark.sql.types import StructType, StructField, IntegerType, StringType
import numpy as np

if __name__ == "__main__":
    """
       Usage: spark code designed for LANL Cyber data, calculates over certain time
        windows: number active nodes, number unique edges, proportion of largest
        connected component of the entire network graph.
    """
    sc = SparkContext()
    spark = SparkSession\
        .builder\
        .appName("LANL_CC")\
        .getOrCreate()

    if(len(sys.argv) != 2):
        sys.exit("Error! Enter name of text file for analysis as sys arg")

    raw_data = sc.textFile(sys.argv[1])


    #get an array with desired time and connection information
    graphInfo = raw_data.map(lambda line: line.split(',')[0:6:2])

    #get the vertices in order to give unique labels
    #unique ids are needed for GraphFrames inputs

    #this gives a dictionary of the unique observed vertices of the form
    # {'Node name 1': id (int), ..... 'Node name (say) 3000': id}

    graph_vertices = graphInfo\
        .flatMap(lambda conn: conn[1:])\
        .distinct()\
        .zipWithUniqueId()\
        .collectAsMap()

    #now: have keys for vertices, need to get corresponding connection array to be
    #input for GraphFrames
    sqlContext = SQLContext(sc)

    #broadcast vertices dictionary to all nodes
    broadcastVertices = sc.broadcast(graph_vertices)

    #Make SQL dataframe to work with GraphFrames

    vertices_df = sc.parallelize([(k,v) for k,v in graph_vertices.items()]).toDF(\
            ['Node Name', 'id'])

    #this now allows us to create a time | src | dst DataFrame with IDs
    #note:
    #for GraphFrames, vertices df must be of form id | other | arbitrary | info|
    # edges df of form | src | dst | other | info
    # MUST HAVE id, src, dst COLS.

    rowFunc = lambda x: (int(x[0]), broadcastVertices.value.get(x[1]), broadcastVertices.value.get(x[2]))

    #mapFunc helps communicate the IDs to the new DataFrame
    def mapFunc(partition):
        for row in partition:
            yield rowFunc(row)


    #map raw data to one with IDs instead. 

    conns_with_IDs = graphInfo.mapPartitions(mapFunc, preservesPartitioning=True)

    #create SQL DataFrame; for easy operations and inputting to GraphFrames
    conns_with_IDs = sqlContext.createDataFrame(conns_with_IDs, ["time", "src", "dst"])

    
    #main function for computing largest Connected Component size
    def largestCC(conn_matrix):
        vertices_df = conn_matrix.rdd.flatMap(lambda x: x[1:]).distinct()
        row = Row('id')
        unique_vertices = vertices_df.map(row).toDF()
        conn_graph = GraphFrame(unique_vertices, conn_matrix)
        conn_comp_size = conn_graph.connectedComponents().groupBy("component")\
                .count()
        largest_cc_size = conn_comp_size.select("count")\
                .sort(func.col("count").desc())
        largest_cc_size = largest_cc_size.select(func.col("count").alias("LCCsize"))
        largest_cc_size = largest_cc_size.take(1)[0]
        num_vertices = vertices_df.count()
        return(float(largest_cc_size.LCCsize)/float(num_vertices))

    def num_vert(conn_matrix):
        vertices = conn_matrix.rdd.flatMap(lambda x: x[1:]).distinct()
        return(vertices.count())

    
    #parameterise calculations
        
    start_time = conns_with_IDs.select(func.col('time')).take(1)[0]['time']
    end_time = conns_with_IDs.select(func.col('time')).collect()[conns_with_IDs.count()-1]['time']
    window_size = 300
    time_splits = np.arange(start_time, end_time, window_size)
    bins = [[str(i),str(i+window_size)] for i in time_splits]
    
    #create appropriate data structure for input to largestCC
    schema = StructType([
            StructField("time", StringType(), True), StructField("src", StringType(),False), StructField("dst", StringType(), False) ])

    conns_matrices_dict = {str(elem) : spark.createDataFrame([],schema) for elem in time_splits }

    for key in conns_matrices_dict.keys():
            conns_matrices_dict[key] = conns_with_IDs.filter(conns_with_IDs.time \
                    > int(key)).filter(conns_with_IDs.time < (int(key)+window_size))

    
    largestCC_sizes = np.zeros(len(time_splits))
    for idx,times  in enumerate(time_splits):
        largestCC_sizes[idx] = largestCC(conns_matrices_dict[str(times)])

    print(largestCC_sizes)

 


    spark.stop()


